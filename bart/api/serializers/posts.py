from rest_framework import serializers
from ..models.bundles import Bundle
from ..models.resources import Resource
from ..models.posts import Post, Attachment, Statistics
from .resources import ResourceReadSerializer



class StatisticsReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Statistics
        fields = ('likes', 'reposts', 'comments', 'views',)


class AttachmentReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Attachment
        fields = ('attachment_type', 'title', 'description', 'preview', 'source')


class PostReadSerializer(serializers.ModelSerializer):
    resource = ResourceReadSerializer()
    statistics = StatisticsReadSerializer()
    attachments = AttachmentReadSerializer(many=True)
    class Meta:
        model = Post

