from django.utils import timezone

from rest_framework import serializers
from ..models.barters import Barter
from ..serializers.posts import PostReadSerializer
from ..serializers.bundles import BundleReadSerializer
from ..models.bundles import Bundle
from ..models.posts import Post
from ..models.resources import Resource

class BarterReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Barter



class BarterCreateSerializer(serializers.ModelSerializer):
    publish_datetime = serializers.DateTimeField(required=False)


    class Meta:
        model = Barter
        fields = ('publish_datetime', 'bundle', 'origin_post', 'destiny_resource')

    def validate_publish_datetime(self, value):
        print(value)
        now = timezone.now()
        if value < now:
            raise serializers.ValidationError("Incorrect time")
        return value

    def validate_destiny_resource(self, value):
        request = self.context.get('request')
        if value.owner != request.user:
            raise serializers.ValidationError("You don't have permissions to use this resource as destiny.")
        return value

    def validate(self, attrs):
        bundle = attrs.get('bundle')
        destiny_resource = attrs.get('destiny_resource')
        publish_datetime = attrs.get('publish_datetime')
        origin_post = attrs.get('origin_post')
        resources_of_selected_bundle = bundle.resources.all()
        if not publish_datetime and not destiny_resource.schedule:
            raise serializers.ValidationError("Publish time is not selected and schedule isn't configured.")
        if not destiny_resource in resources_of_selected_bundle:
            raise serializers.ValidationError("Resource is not a member of selected bundle.")
        if not origin_post.resource in resources_of_selected_bundle:
            raise serializers.ValidationError("Post doesn't belong to a resource of selected bundle.")
        return attrs

    def create(self, validated_data):

        def select_nearest_free_time(resource):
            now = timezone.now()
            one_day = timezone.timedelta(days=1)

            # searching for last planned barter
            # to put our next barter after it according to a schedule
            last_barter = Barter.objects.filter(
                destiny_resource=destiny_resource,
                publish_datetime__gt=now
            ).last()

            # if there will be no barters in future
            # we select current day as countdown start
            if last_barter:
                countdown_start = last_barter.publish_datetime
            else:
                countdown_start = now
            schedule = resource.schedule
            schedule.sort()

            publish_time = None
            # trying to find free space within current day
            for time in schedule:
                if time > countdown_start.time():
                    publish_time = timezone.datetime.combine(
                        time=time, date=countdown_start
                    )
                    break
            # if all slots of current day are busy
            # we take first slot of the next day
            if not publish_time:
                publish_time = timezone.datetime.combine(
                    time=schedule[0], date=countdown_start+one_day
                )
            return publish_time

        internal_data = self.to_internal_value(validated_data)

        bundle = internal_data.get('bundle')
        destiny_resource = internal_data.get('destiny_resource')
        origin_post = internal_data.get('origin_post')
        publish_datetime = validated_data.get('publish_datetime')

        if not publish_datetime:
            publish_datetime = select_nearest_free_time(resource=destiny_resource)

        return Barter.objects.create(
            destiny_resource=destiny_resource,
            origin_post=origin_post,
            origin_resource=origin_post.resource,
            publish_datetime=publish_datetime,
            bundle=bundle
        )



class BarterUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Barter

    def update(self, instance, validated_data):
        pass