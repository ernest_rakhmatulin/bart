from bart.api.models.resources import Resource
from rest_framework import serializers
from django.db import transaction
from ..utils import ResourceConverter

class ResourceCreateSerializer(serializers.Serializer):
    url = serializers.URLField(max_length=200, allow_blank=False)


    def validate(self, data):
        validation_fields_list = ['internal_id', 'internal_type', 'network', 'owner']
        prepared_resource_data = self.resource_converter.prepared_resource_data
        filter_parameters = {
            k:prepared_resource_data[k] for k in validation_fields_list
            if k in prepared_resource_data
            }
        resource_already_exists = Resource.objects.filter(**filter_parameters).exists()
        if resource_already_exists:
            raise serializers.ValidationError("Resource already added to your account.")
        return data

    def validate_url(self, value):
        request = self.context.get('request')
        self.resource_converter = ResourceConverter(owner=request.user, url=value)
        resource_data = self.resource_converter.prepared_resource_data
        if not resource_data:
            raise serializers.ValidationError("Resource doesn't exist or you are not an administrator")
        return value

    def create(self, validated_data):
        request = self.context.get('request')
        resource_data = self.resource_converter.prepared_resource_data
        with transaction.atomic():
            resource, created = Resource.objects.get_or_create(**resource_data)
        return resource


class ResourceReadSerializer(serializers.ModelSerializer):
    image = serializers.URLField(source='internal_image')
    class Meta:
        model = Resource
        fields = ('id', 'title', 'network', 'page', 'image', 'schedule')


class ResourceUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Resource
        fields = ('schedule',)

    def validate_schedule(self, value):
        if len(value) != len(set(value)):
            raise serializers.ValidationError("Time within a day must be unique.")
        return value