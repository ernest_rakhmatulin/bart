from rest_framework import serializers
from ..models.bundles import Bundle, Application, Settings, Category
from ..models.resources import Resource
from ..serializers.resources import ResourceReadSerializer
from ..constants import STATUS_APPROVED, STATUS_PENDING, STATUS_DECLINED


class CategoryReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category


class SettingsReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Settings
        fields = ('measure', 'skip_reposts', 'skip_ads')

class SettingsUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Settings
        fields = ('measure', 'skip_reposts', 'skip_ads')


class BundleReadSerializer(serializers.ModelSerializer):
    resources = ResourceReadSerializer(many=True)
    category = CategoryReadSerializer()

    class Meta:
        model = Bundle
        fields = ('id', 'title', 'network', 'owner', 'resources', 'category', 'private')


class BundleCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Bundle
        fields = ('title', 'network', 'category', 'private')


    def create(self, validated_data):
        request = self.context.get('request')
        validated_data['owner'] = request.user
        validated_data['category'] = Category.objects.get(id=validated_data['category'])
        bundle = Bundle(**validated_data)
        bundle.save()
        settings = Settings(bundle=bundle)
        settings.save()
        return bundle

class BundleUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Bundle
        fields = ('title', 'category', 'private')


class ApplicationUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Application
        fields = ('status',)

    def update(self, application, validated_data):
        application.status = validated_data['status']
        application.save()
        if application.status == STATUS_APPROVED:
            application.bundle.resources.add(application.resource)
        return application


class ApplicationReadSerializer(serializers.ModelSerializer):

    resource = ResourceReadSerializer()
    bundle = BundleReadSerializer()

    class Meta:
        model = Application
        fields = ('id', 'resource', 'bundle', 'status',)


class ApplicationCreateSerializer(serializers.Serializer):

    resource = serializers.CharField()
    bundle = serializers.CharField()

    def create(self, validated_data):
        request = self.context.get('request')
        resource = Resource.objects.get(id=validated_data['resource'], owner=request.user)
        bundle = Bundle.objects.get(id=validated_data['bundle'])
        application, created = Application.objects.get_or_create(
            resource=resource,
            bundle=bundle,
            status=STATUS_PENDING
        )
        return application