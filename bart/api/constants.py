#models.bundle
#models.resources
NETWORK_VK = 'vk.com'
NETWORK_CHOICES = (
    (NETWORK_VK, NETWORK_VK),
)

#models.bundle
MEASURE_VIEWS = 'views'
MEASURE_REPOSTS = 'reposts'
MEASURE_CHOICES = (
    (MEASURE_VIEWS, MEASURE_VIEWS),
    (MEASURE_REPOSTS, MEASURE_REPOSTS),
)

# models.application
STATUS_PENDING = 'pending'
STATUS_APPROVED = 'approved'
STATUS_DECLINED = 'declined'
STATUS_CHOICES = (
    (STATUS_PENDING, STATUS_PENDING),
    (STATUS_APPROVED, STATUS_APPROVED),
    (STATUS_DECLINED, STATUS_DECLINED),
)

#models.posts
TYPE_PHOTO = 'photo'
TYPE_VIDEO = 'video'
TYPE_AUDIO = 'audio'
TYPE_DOCUMENT = 'doc'
TYPE_LINK = 'link'
TYPE_CHOICES = (
    (TYPE_PHOTO, TYPE_PHOTO),
    (TYPE_VIDEO, TYPE_VIDEO),
    (TYPE_AUDIO, TYPE_AUDIO),
    (TYPE_DOCUMENT, TYPE_DOCUMENT),
    (TYPE_LINK, TYPE_LINK),
)

weekdays = [
    'monday', 'tuesday', 'wednesday', 'thursday',
    'friday', 'saturday', 'sunday'
]