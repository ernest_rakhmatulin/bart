import uuid
from django.db import models
from ..models.posts import Post
from ..models.resources import Resource
from ..models.bundles import Bundle

class Barter(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    origin_post = models.ForeignKey(Post, related_name='origin_post_barters')
    destiny_post = models.ForeignKey(Post, related_name='destiny_post_barters', null=True, blank=True)
    origin_resource = models.ForeignKey(Resource,  related_name='origin_resource_barters')
    destiny_resource = models.ForeignKey(Resource,  related_name='destiny_resource_barters')
    bundle = models.ForeignKey(Bundle)
    publish_datetime = models.DateTimeField()