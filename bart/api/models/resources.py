import uuid
from django.db import models
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from ..constants import NETWORK_CHOICES
from django.contrib.postgres.fields import ArrayField

class Resource(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    internal_id = models.CharField(null=True, max_length=50)
    internal_type = models.CharField(max_length=15, null=True, blank=True)
    internal_image = models.URLField(blank=True, null=True)
    title = models.CharField(max_length=50)
    network = models.CharField(max_length=50, choices=NETWORK_CHOICES)
    page = models.CharField(max_length=50)
    owner = models.ForeignKey(User)
    schedule = ArrayField(models.TimeField(), null=True, blank=True)

    def __str__(self):
        return '%s - %s/%s' % (self.title, self.network, self.page)
