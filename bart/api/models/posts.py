import uuid
from django.db import models
from .resources import Resource
from ..constants import TYPE_CHOICES


class Post(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    internal_id = models.IntegerField(null=True)
    resource = models.ForeignKey(Resource)
    created_at = models.DateTimeField()
    text = models.TextField()
    deleted = models.BooleanField(default=False)

    class Meta:
        ordering = ['-internal_id']

class Statistics(models.Model):
    likes = models.IntegerField(default=0)
    reposts = models.IntegerField(default=0)
    comments = models.IntegerField(default=0)
    views = models.IntegerField(default=0)
    post = models.ForeignKey(Post, related_name='statistics')


class Attachment(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    attachment_type = models.CharField(choices=TYPE_CHOICES, max_length=10)
    title = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    preview = models.URLField(blank=True, null=True, max_length=255)
    source = models.URLField(blank=True, null=True, max_length=255)
    post = models.ForeignKey(Post, related_name='attachments')