import uuid
from django.db import models
from django.contrib.auth.models import User
from .resources import Resource
from ..constants import NETWORK_CHOICES, MEASURE_CHOICES, STATUS_CHOICES, MEASURE_VIEWS


class Category(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=50)


class Bundle(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=50)
    network = models.CharField(max_length=50, choices=NETWORK_CHOICES)
    resources = models.ManyToManyField(Resource, blank=True)
    owner = models.ForeignKey(User)
    private = models.BooleanField(default=True)
    category = models.ForeignKey(Category)

    def __str__(self):
        return '%s - %s' % (self.title, self.network)


class Application(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    resource = models.ForeignKey(Resource)
    bundle = models.ForeignKey(Bundle)
    status = models.CharField(max_length=50, choices=STATUS_CHOICES)


class Settings(models.Model):
    bundle = models.ForeignKey(Bundle, related_name='settings')
    measure = models.CharField(max_length=50, choices=MEASURE_CHOICES, default=MEASURE_VIEWS)
    skip_reposts = models.BooleanField(default=True)
    skip_ads = models.BooleanField(default=True)