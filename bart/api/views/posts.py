from rest_framework.authentication import BasicAuthentication
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from bart.api.models.posts import Post
from bart.api.serializers.posts import PostReadSerializer


class PostList(APIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)
    pagination_class = LimitOffsetPagination

    def get_queryset(self):
        if 'bundle' in self.request.GET:
            return Post.objects.filter(
                resource__bundle__id=self.request.GET['bundle']
            )
        elif 'resource' in self.request.GET:
            return Post.objects.filter(
                resource_id=self.request.GET['resource']
            )
        else:
            return []

    def get(self, request, format=None):
        posts = self.get_queryset()
        paginator = self.pagination_class()
        posts_page = paginator.paginate_queryset(posts, request)
        read_serializer = PostReadSerializer(posts_page, many=True)
        return paginator.get_paginated_response(read_serializer.data)

