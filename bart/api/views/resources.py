from bart.api.serializers.resources import ResourceCreateSerializer, ResourceReadSerializer, ResourceUpdateSerializer
from bart.api.models.resources import Resource
from django.http import Http404
from rest_framework import status
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from ..utils import PostSyncMaster
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.throttling import ScopedRateThrottle


class ResourceList(APIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)
    pagination_class = LimitOffsetPagination

    def get_queryset(self):
        return Resource.objects.filter(owner=self.request.user)

    def get(self, request, format=None):
        resources = self.get_queryset()
        paginator = self.pagination_class()
        resources_page = paginator.paginate_queryset(resources, request)
        read_serializer = ResourceReadSerializer(resources_page, many=True)
        return paginator.get_paginated_response(read_serializer.data)

    def post(self, request, format=None):
        create_serializer = ResourceCreateSerializer(
            data=request.data, context={'request': request}
        )
        if create_serializer.is_valid():
            resource = create_serializer.create(create_serializer.data)
            read_serializer = ResourceReadSerializer(instance=resource)
            return Response(read_serializer.data, status=status.HTTP_201_CREATED)
        return Response(create_serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class ResourceDetail(APIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_object(self, pk):
        try:
            return Resource.objects.get(pk=pk, owner=self.request.user)
        except Resource.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        resource = self.get_object(pk)
        serializer = ResourceReadSerializer(resource)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        resource = self.get_object(pk)
        update_serializer = ResourceUpdateSerializer(resource, data=request.data)
        if update_serializer.is_valid():
            update_serializer.update(resource, update_serializer.validated_data)
            read_serializer = ResourceReadSerializer(instance=resource)
            return Response(read_serializer.data, status=status.HTTP_201_CREATED)
        return Response(update_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ResourceSyncStats(APIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)
    throttle_classes = (ScopedRateThrottle,)
    throttle_scope = 'sync'

    def get_object(self, pk):
        try:
            return Resource.objects.get(pk=pk, owner=self.request.user)
        except Resource.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        resource = self.get_object(pk)
        post_sync_master = PostSyncMaster(resource)
        post_sync_master.sync()
        return Response(status=status.HTTP_200_OK)
