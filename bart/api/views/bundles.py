from ..serializers.bundles import BundleReadSerializer, BundleCreateSerializer, \
    BundleUpdateSerializer, ApplicationUpdateSerializer, ApplicationReadSerializer, \
    ApplicationCreateSerializer, SettingsUpdateSerializer, SettingsReadSerializer, CategoryReadSerializer
from ..models.bundles import Bundle, Application, Category

from django.http import Http404
from rest_framework import status
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.pagination import LimitOffsetPagination

class BundleList(APIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)
    pagination_class = LimitOffsetPagination

    def get(self, request, format=None):
        resources = Bundle.objects.filter(owner=request.user)
        paginator = self.pagination_class()
        bundles_page = paginator.paginate_queryset(resources, request)
        read_serializer = BundleReadSerializer(bundles_page, many=True)
        return paginator.get_paginated_response(read_serializer.data)

    def post(self, request, format=None):
        create_serializer = BundleCreateSerializer(
            data=request.data, context={'request': request}
        )
        if create_serializer.is_valid():
            bundle = create_serializer.create(create_serializer.data)
            read_serializer = BundleReadSerializer(instance=bundle)
            return Response(read_serializer.data, status=status.HTTP_201_CREATED)
        return Response(create_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BundleDetail(APIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_object(self, pk):
        try:
            return Bundle.objects.get(pk=pk, owner=self.request.user)
        except Bundle.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        resource = self.get_object(pk)
        serializer = BundleReadSerializer(resource)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        schedule = self.get_object(pk)
        update_serializer = BundleUpdateSerializer(schedule, data=request.data)
        update_serializer.is_valid(raise_exception=True)
        validated_data = update_serializer.validated_data
        bundle = update_serializer.update(schedule, validated_data=validated_data)
        read_serializer = BundleReadSerializer(instance=bundle)
        return Response(read_serializer.data)

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



class SettingsDetail(APIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_object(self, pk):
        try:
            bundle = Bundle.objects.get(pk=pk, owner=self.request.user)
            settings = bundle.settings.get()
            return settings
        except Bundle.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        settings = self.get_object(pk)
        serializer = SettingsReadSerializer(settings)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        settings = self.get_object(pk)
        update_serializer = SettingsUpdateSerializer(settings, data=request.data)
        update_serializer.is_valid(raise_exception=True)
        validated_data = update_serializer.validated_data
        settings = update_serializer.update(settings, validated_data=validated_data)
        read_serializer = SettingsReadSerializer(instance=settings)
        return Response(read_serializer.data)


class ApplicationDetail(APIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_object(self, pk):
        try:
            application = Application.objects.get(pk=pk, bundle__owner=self.request.user)
            return application
        except Application.DoesNotExist:
            raise Http404

    def put(self, request, pk, format=None):
        application = self.get_object(pk)
        update_serializer = ApplicationUpdateSerializer(application, data=request.data)
        update_serializer.is_valid(raise_exception=True)
        validated_data = update_serializer.validated_data
        application = update_serializer.update(application, validated_data=validated_data)
        read_serializer = ApplicationReadSerializer(instance=application)
        return Response(read_serializer.data)

    def delete(self, request, pk, format=None):
        application = self.get_object(pk)
        application.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class CategoryList(APIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)
    pagination_class = LimitOffsetPagination

    def get_queryset(self):
        return Category.objects.all()

    def get(self, request, format=None):
        queryset = self.get_queryset()
        paginator = self.pagination_class()
        applications_page = paginator.paginate_queryset(queryset, request)
        read_serializer = CategoryReadSerializer(applications_page, many=True)
        return paginator.get_paginated_response(read_serializer.data)


class ApplicationList(APIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)
    pagination_class = LimitOffsetPagination

    def get_queryset(self):
        queryset = Application.objects.all()
        show = self.request.GET.get('show')
        application_status = self.request.GET.get('status')

        if show == 'their':
            queryset = queryset.filter(resource__owner=self.request.user)
        elif show == 'my':
            queryset = queryset.filter(bundle__owner=self.request.user)
        else:
            queryset = queryset.filter(bundle__owner=self.request.user)
        if application_status:
            queryset = queryset.filter(status=application_status)

        return queryset

    def post(self, request, format=None):
        create_serializer = ApplicationCreateSerializer(
            data=request.data, context={'request': request}
        )
        if create_serializer.is_valid():
            bundle = create_serializer.create(create_serializer.data)
            read_serializer = ApplicationReadSerializer(instance=bundle)
            return Response(read_serializer.data, status=status.HTTP_201_CREATED)
        return Response(create_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, format=None):
        queryset = self.get_queryset()
        paginator = self.pagination_class()
        applications_page = paginator.paginate_queryset(queryset, request)
        read_serializer = ApplicationReadSerializer(applications_page, many=True)
        return paginator.get_paginated_response(read_serializer.data)
