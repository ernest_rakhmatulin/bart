from ..models.barters import Barter
from ..models.resources import Resource
from ..serializers.barters import BarterReadSerializer, BarterCreateSerializer, BarterUpdateSerializer

from django.http import Http404
from rest_framework import status
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.exceptions import PermissionDenied



class BartersList(APIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)
    pagination_class = LimitOffsetPagination

    def get_queryset(self):

        queryset = Barter.objects.none()

        bundle_id = self.request.GET.get('bundle')
        destiny_resource_id = self.request.GET.get('destiny_resource')

        users_resource_added_to_bundle = Resource.objects.filter(
            owner=self.request.user, bundle__id=bundle_id
        ).exists()

        if bundle_id and not users_resource_added_to_bundle:
            raise PermissionDenied("You can't see barters in this bundle.")

        if any([bundle_id, destiny_resource_id]):
            queryset = Barter.objects.all()
            if bundle_id:
                queryset = queryset.filter(bundle_id=bundle_id)
            if destiny_resource_id:
                queryset = queryset.filter(destiny_resource_id=destiny_resource_id)

        return queryset


    def get(self, request, format=None):
        resources = self.get_queryset()
        paginator = self.pagination_class()
        resources_page = paginator.paginate_queryset(resources, request)
        read_serializer = BarterReadSerializer(resources_page, many=True)
        return paginator.get_paginated_response(read_serializer.data)


    def post(self, request, format=None):
        create_serializer = BarterCreateSerializer(
            data=request.data, context={'request': request}
        )
        if create_serializer.is_valid():
            bundle = create_serializer.create(create_serializer.data)
            read_serializer = BarterReadSerializer(instance=bundle)
            return Response(read_serializer.data, status=status.HTTP_201_CREATED)
        return Response(create_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BartersDetail(APIView):

    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)


    def get_object(self, pk):
        try:
            return Barter.objects.get(pk=pk)
        except Barter.DoesNotExist:
            raise Http404


    def get(self, request, pk, format=None):
        resource = self.get_object(pk)
        serializer = BarterReadSerializer(resource)
        return Response(serializer.data)


    def put(self, request, pk, format=None):
        schedule = self.get_object(pk)
        update_serializer = BarterUpdateSerializer(schedule, data=request.data)
        update_serializer.is_valid(raise_exception=True)
        validated_data = update_serializer.validated_data
        bundle = update_serializer.update(schedule, validated_data=validated_data)
        read_serializer = BarterReadSerializer(instance=bundle)
        return Response(read_serializer.data)


    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)