import re
import vk
from vk.exceptions import VkAPIError
import time
import datetime
from django.conf import settings
from urllib.parse import urlparse

from .models.posts import Post, Attachment, Statistics
from .constants import NETWORK_VK, TYPE_AUDIO, TYPE_PHOTO, TYPE_VIDEO, TYPE_DOCUMENT, TYPE_LINK


class APIClientsCreator:

    @staticmethod
    def vk_api(access_token):
        session = vk.Session()
        return vk.API(
            session=session,
            access_token=access_token,
            v=settings.VK_API_VERSION
        )


class ResourceConverter:
    # TODO: Add validation logic and generation of error messages

    def __init__(self, url, owner):
        self.parsed_url = urlparse(url)
        self.network = self.parsed_url.netloc
        self.page = self.parsed_url.path.strip('/')
        self.owner = owner
        self.prepared_resource_data = self.resource_data()

    def resource_data(self):
        if self.network == NETWORK_VK:
            return self._resource_data_vk()

    def _resource_data_vk(self):
        default_resouce_page_regex = re.compile('^public[0-9]+$')
        match = default_resouce_page_regex.match(self.page)
        access_token = self.owner.social_auth.get(
            provider='vk-oauth2'
        ).extra_data.get('access_token')
        vk_api = APIClientsCreator.vk_api(access_token=access_token)
        if match:
            self.page = self.page.replace('public', 'club')
        group = vk_api.groups.getById(group_id=self.page).pop()
        if group.get('is_admin'):
            if group.get('type') in ['group', 'page']:
                group['id'] = -group['id']
            return {
                'network': self.network,
                'page': self.page,
                'title': group.get('name'),
                'owner': self.owner,
                'internal_id': str(group.get('id')),
                'internal_type': group.get('type'),
                'internal_image': group.get('photo_200')
            }
        else:
            return None


class PostSyncMaster:

    def __init__(self, resource, post_depth=10, stats_depth=100):
        self.resource = resource
        self.owner = resource.owner
        self.post_depth = post_depth
        self.stats_depth = stats_depth
        if resource.network == NETWORK_VK:
            access_token = self.owner.social_auth.get(
                provider='vk-oauth2'
            ).extra_data.get('access_token')
            self.api = APIClientsCreator.vk_api(access_token)

    def make_api_call(self, method, parameters, raise_exception=False):
        if self.resource.network == NETWORK_VK:
            try:
                attributes = method.split('.')
                obj = self.api
                for attribute in attributes:
                    obj = getattr(obj, attribute)
                    if attribute == attributes[-1]:
                        return obj(**parameters)
            except VkAPIError as e:
                if raise_exception:
                    raise e
                else:
                    time.sleep(5)
                    return self.make_api_call(
                        method=method,
                        parameters=parameters,
                        raise_exception=True
                    )


    def sync(self):
        if self.resource.network == NETWORK_VK:
            self._sync_stats_vk()

    def _sync_stats_vk(self):
        vk_posts = self.make_api_call(
            method='wall.get', parameters={
                'owner_id': self.resource.internal_id,
                'count': self.stats_depth
            }
        )
        self._pull_posts_vk(pulled_posts=vk_posts)
        posts = Post.objects.filter(resource=self.resource)[:self.stats_depth]
        id_post_dict = {}
        for post_data in vk_posts['items']:
            id_post_dict[post_data['id']] = post_data

        for post in posts:
            if post.internal_id in id_post_dict:
                statistics = post.statistics.get()
                statistics.views = id_post_dict[post.internal_id]['views']['count']
                statistics.likes = id_post_dict[post.internal_id]['likes']['count']
                statistics.reposts = id_post_dict[post.internal_id]['reposts']['count']
                statistics.comments = id_post_dict[post.internal_id]['comments']['count']
                statistics.save()
            else:
                post.deleted = True
                post.save()

    def _vk_post_valid(self, post_item):
        if post_item['from_id'] != post_item['owner_id']:
            return False
        if 'marked_as_ads' in post_item and post_item['marked_as_ads']:
            return False
        elif 'copy_history' in post_item:
            return False
        return True

    def _pull_posts_vk(self, pulled_posts=None):
        last_post_parsed = Post.objects.filter(resource=self.resource).first()
        if pulled_posts:
            vk_posts = pulled_posts
        else:
            vk_posts = self.make_api_call(
                method='wall.get', parameters={
                    'owner_id': self.resource.internal_id,
                    'count': self.stats_depth
                }
            )

        for post_data in vk_posts['items']:
            if last_post_parsed and post_data['id'] <= last_post_parsed.internal_id:
                break
            if self._vk_post_valid(post_data):

                prepared_data = {
                    'internal_id': post_data['id'],
                    'resource': self.resource,
                    'created_at': datetime.datetime.fromtimestamp(post_data['date']),
                    'text': post_data['text'],
                }

                post = Post(**prepared_data)
                post.save()
                statistics = Statistics(
                    views=post_data['views']['count'],
                    likes=post_data['likes']['count'],
                    reposts=post_data['reposts']['count'],
                    comments=post_data['comments']['count'],
                    post=post
                )
                statistics.save()

                if 'attachments' in post_data:
                    prepared_attachments = []
                    for attachment in post_data['attachments']:
                        if attachment['type'] == TYPE_PHOTO:
                            prepared_attachments.append({
                                'source': attachment['photo']['photo_604'],
                                'preview': attachment['photo']['photo_604'],
                                'description': attachment['photo']['text'],
                                'attachment_type': TYPE_PHOTO,
                                'post': post
                            })
                        if attachment['type'] == TYPE_LINK:

                            prepared_attachment_data = {
                                'source': attachment['link']['url'],
                                'description': attachment['link']['description'],
                                'title': attachment['link']['title'],
                                'attachment_type': TYPE_LINK,
                                'post': post
                            }
                            if 'photo' in attachment:
                                prepared_attachment_data['preview'] = attachment['photo']['photo_604']
                            prepared_attachments.append(
                                prepared_attachment_data
                            )
                        if attachment['type'] == TYPE_VIDEO:
                            video_id = '%s_%s_%s' % (
                                attachment['video']['owner_id'],
                                attachment['video']['id'],
                                attachment['video']['access_key']
                            )
                            video_data = self.make_api_call(
                                method='video.get',
                                parameters={'videos': video_id}
                            )
                            video_data = video_data['items'][0]
                            prepared_attachments.append({
                                'source': video_data['player'],
                                'preview': video_data['photo_320'],
                                'title': video_data['title'],
                                'post': post,
                                'description': video_data['description'],
                                'attachment_type': TYPE_VIDEO,
                            })
                        if attachment['type'] == TYPE_DOCUMENT:
                            prepared_attachments.append({
                                'source': attachment['doc']['url'],
                                'title': attachment['doc']['title'],
                                'post': post,
                                'attachment_type': TYPE_DOCUMENT,
                            })
                        if attachment['type'] == TYPE_AUDIO:
                            title = '%s - %s' % (
                                attachment['audio']['artist'],
                                attachment['audio']['title']
                            )
                            prepared_attachments.append({
                                'title': title,
                                'post': post,
                                'attachment_type': TYPE_AUDIO,
                            })
                    for attachment in prepared_attachments:
                        # TODO: Update exception logic
                        try:
                            Attachment(**attachment).save()
                        except Exception as e:
                            print(e.args)
                            pass
                        # Attachment.objects.bulk_create(attachments)