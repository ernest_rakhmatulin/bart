from django.contrib import admin
from .models.resources import Resource
from .models.bundles import Bundle, Application, Settings, Category
from .models.posts import Attachment, Post, Statistics
from .models.barters import Barter

class StatisticsInline(admin.TabularInline):
    model = Statistics
    can_delete = False
    readonly_fields = ('views', 'likes', 'reposts', 'comments')
    extra = 0


class AttachmentsInline(admin.StackedInline):
    model = Attachment
    extra = 0

class PostAdmin(admin.ModelAdmin):
    inlines = [AttachmentsInline, StatisticsInline]

admin.site.register(Resource)
admin.site.register(Bundle)
admin.site.register(Application)
admin.site.register(Settings)
admin.site.register(Attachment)
admin.site.register(Post, PostAdmin)
admin.site.register(Statistics)
admin.site.register(Barter)
admin.site.register(Category)


