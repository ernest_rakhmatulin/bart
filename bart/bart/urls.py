"""bart URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from bart.api.views.resources import ResourceDetail, ResourceList, \
    ResourceSyncStats
from bart.api.views.bundles import BundleDetail, BundleList, ApplicationDetail, ApplicationList, SettingsDetail, CategoryList
from bart.api.views.posts import PostList
from bart.api.views.barters import BartersList, BartersDetail

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^resources/$', ResourceList.as_view()),
    url(r'^resources/(?P<pk>[0-9a-f-]+)/$', ResourceDetail.as_view()),
    url(r'^resources/(?P<pk>[0-9a-f-]+)/sync/$', ResourceSyncStats.as_view()),

    url(r'^categories/$', CategoryList.as_view()),
    url(r'^bundles/$', BundleList.as_view()),
    url(r'^bundles/(?P<pk>[0-9a-f-]+)/$', BundleDetail.as_view()),
    url(r'^bundles/(?P<pk>[0-9a-f-]+)/settings/$', SettingsDetail.as_view()),

    url(r'^barters/$', BartersList.as_view()),
    url(r'^barters/(?P<pk>[0-9a-f-]+)/$', BartersDetail.as_view()),

    url(r'^applications/$', ApplicationList.as_view()),
    url(r'^applications/(?P<pk>[0-9a-f-]+)/$', ApplicationDetail.as_view()),

    url(r'^posts/$', PostList.as_view()),

    url('', include('social_django.urls', namespace='social'))
]
